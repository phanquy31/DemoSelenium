package Microsoft.Steps;

import Microsoft.Pages.HomePage;
import Microsoft.Pages.LoginPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginSteps {

    @Given("Open url and go to login page")
    public void openUrlAndGoToLoginPage() {
        HomePage homePage = new HomePage(true);
        homePage.goToLoginPage();
    }

    @When("Enter my user name {string}")
    public void enterMyUserName(String userName) {
        LoginPage loginPage = new LoginPage(true);
        loginPage.fillUserName(userName);
    }

    @Then("Verify text alert is {string} and color code {string}")
    public void verifyTextAlertIsWithColorCode(String textAlert, String colorCode) {
        LoginPage loginPage = new LoginPage(true);
        loginPage.verifyAlert(textAlert,colorCode);
    }

    @And("Enter my password {string}")
    public void enterMyPassword(String password) {
        LoginPage loginPage = new LoginPage(true);
        loginPage.fillPassword(password);
    }

}
