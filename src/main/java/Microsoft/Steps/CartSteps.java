package Microsoft.Steps;

import Microsoft.Pages.CartPage;
import Microsoft.Pages.HomePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.testng.Assert;

public class CartSteps {

    @Given("Open url and hover mouse into cart icon")
    public void openUrlAndHoverMouseIntoCartIcon() {
        HomePage homePage = new HomePage(true);
        homePage.hoverMouseIntoCartButton();
    }

    @And("See show content is {string}")
    public void seeTextOverPlaceIs(String content) {
        HomePage homePage = new HomePage(true);
        Assert.assertEquals(homePage.getContentCart(),content,String.format("Failed, text expect is % but show %s...",content, homePage.getContentCart()));
    }

    @And("Click to cart detail")
    public void clickToCartDetail() {
        HomePage homePage = new HomePage(true);
        homePage.clickButtonCart();
    }

    @Then("Detail description show text {string}")
    public void detailDescriptionShowText(String textExpect) {
        CartPage cartPage = new CartPage(true);
        cartPage.verifyCartEmpty(textExpect);
    }

}
