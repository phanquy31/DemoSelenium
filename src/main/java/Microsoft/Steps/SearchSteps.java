package Microsoft.Steps;

import Core.Support.Selenium.BrowserManager;
import Microsoft.Pages.HomePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

public class SearchSteps {
    @Given("Open url and click to search")
    public void openUrlAndClickToSearch() {
        HomePage homePage = new HomePage(true);
        homePage.clickIconSearch();
    }

    @When("Search product with key {string}")
    public void searchProductWithKey(String key) {
        HomePage homePage = new HomePage(true);
        homePage.searchWithKey(key);
    }

    @Then("Verify the product name {string} exists in the suggested products list")
    public void verifyTheProductNameExistsInTheSuggestedProductsList(String productName) {
        boolean exist = false;
        HomePage homePage = new HomePage(true);
        homePage.getProductList();
        for (String product : homePage.getProductList()) {
            if (product.equalsIgnoreCase(productName)){
                exist = true;
            }
        }
        Assert.assertTrue(exist,"Failed, product not exist into list....");
    }

    @And("Click search and go to search page link {string}")
    public void clickSearchAndGoToSearchPage(String link) {
        HomePage homePage = new HomePage(true);
        homePage.clickIconSearch();
        String searchPage = BrowserManager.getInstance().getCurrentUrl();
        Assert.assertEquals(searchPage,link,String.format("Failed, Text expect %s but found %s",link,searchPage));
    }
}
