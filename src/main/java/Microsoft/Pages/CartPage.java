package Microsoft.Pages;

import Core.Selenium.Label;
import Core.Selenium.Textbox;
import org.openqa.selenium.By;
import org.testng.Assert;

public class CartPage extends BasePage{
    private static final By by = new By.ByXPath("//a[contains(.,'Keep shopping']");
    private static final String name = "Cart page";

    // locator
    private final String xpath_labelCartEmpty = "//div[@class='store-cart-app']//p[contains(.,'Your cart is empty.')]";


    //element
    private final Label labelCartEmpty = new Label(By.xpath(xpath_labelCartEmpty),"empty cart");

    public CartPage(boolean assertOpen) {
        super(by, name, assertOpen);
    }

    public void verifyCartEmpty(String expect){
        String actual = labelCartEmpty.getText();
        Assert.assertEquals(actual,expect, String.format("Failed, expect see text %s but see %s...",expect,actual));
    }
}
