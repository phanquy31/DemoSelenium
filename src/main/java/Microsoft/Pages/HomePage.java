package Microsoft.Pages;

import Core.Selenium.*;
import Core.Selenium.BasePage;
import org.openqa.selenium.By;
import org.testng.Assert;

import javax.lang.model.element.Element;
import java.util.ArrayList;
import java.util.List;

public class HomePage extends BasePage {
//    private static final By by = new By.ById("meControl");
    private static final By by = new By.ById("uhf-shopping-cart");
    private static final String name = "home page";

    //locator
    private final String id_buttonLogin = "meControl";
    private final String id_buttonSearch = "search";
    private final String id_textboxSearch = "cli_shellHeaderSearchInput";
    private final String xpath_suggestProduct = "//div[@class='m-auto-suggest']//li[@class='c-menu-item']//div/span";
//    private final String xpath_buttonCart = "//a[@id='uhf-shopping-cart']/preceding-sibling::button";
private final String xpath_buttonCart = "//a[@id='uhf-shopping-cart']";
    private final String xpath_contentCart = "//a[@id='uhf-shopping-cart']/span[@id='uhf-shopping-cart-tooltip']";

    //element
    private final Button buttonLogin = new Button(By.id(id_buttonLogin), "button login");
    private final Button buttonSearch = new Button(By.id(id_buttonSearch), "button search");
    private final Textbox textboxSearch = new Textbox(By.id(id_textboxSearch), "textbox search");
    private final ListOfElements listSuggest = new ListOfElements(By.xpath(xpath_suggestProduct), "suggest product list");
    private final Button buttonCart = new Button(By.xpath(xpath_buttonCart), "button cart");
    private final Label labelContentCart = new Label(By.xpath(xpath_contentCart), "cart content");

    public HomePage(boolean assertPageOpen) {
        super(by, name, assertPageOpen);
    }

    public void goToLoginPage(){
        buttonLogin.waitForClickable();
        buttonLogin.click();
        waitForPageLoadComplete();
    }

    public void clickIconSearch(){
        buttonSearch.waitForClickable();
        buttonSearch.click();
        waitForPageLoadComplete();
    }

    public void searchWithKey(String keyword){
        textboxSearch.waitForElementToBeDisplay();
        textboxSearch.click();
        textboxSearch.sendClearText(keyword);
        waitForJSToComplete();
    }

    public List<String> getProductList(){
        List<String> productList = new ArrayList<>();
        for (int i = 0; i < listSuggest.getListOfElement().size(); i++) {
            productList.add(listSuggest.getListOfElement().get(i).getText());
        }
        return productList;
    }

    public void hoverMouseIntoCartButton(){
        buttonCart.waitForClickable();
        buttonCart.hoverMouse();
        waitForJSToComplete();
    }

    public String getContentCart(){
        labelContentCart.waitForElementToBeDisplay();
        return labelContentCart.getText();
    }

    public void clickButtonCart(){
        buttonCart.waitForClickable();
        buttonCart.click();
        waitForPageLoadComplete();
    }
}
