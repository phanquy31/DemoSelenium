package Microsoft.Pages;

import Core.Selenium.Button;
import Core.Selenium.Label;
import Core.Selenium.Textbox;
import org.openqa.selenium.By;
import org.openqa.selenium.support.Color;
import org.testng.Assert;

public class LoginPage extends BasePage{
    private static final By by = new By.ById("idSIButton9");
    private static final String name = "Login page";

    // locator
    private final String xpath_textBoxUsername = "//input[@name='loginfmt']";
    private final String xpath_alert = "//div[@data-testid='inputComponentWrapper']/div[@role='alert']/div";

    private final String id_buttonNext = "idSIButton9";
    private final String xpath_textBoxPassword = "//input[@name='passwd']";

    //element
    private final Textbox textboxUserName = new Textbox(By.xpath(xpath_textBoxUsername),"account");
    private final Button buttonNext = new Button(By.id(id_buttonNext), "submit");
    private final Label labelAlertError = new Label(By.xpath(xpath_alert), "alert error");
    private final Textbox textboxPasswd = new Textbox(By.xpath(xpath_textBoxPassword), "password");

    public LoginPage(boolean assertOpen) {
        super(by, name, assertOpen);
    }

    public void fillUserName(String userName){
        textboxUserName.waitForElementToBeDisplay();
        textboxUserName.click();
        if (userName!=null){
            textboxUserName.sendClearText(userName);
            waitForJSToComplete();
        }
        buttonNext.click();
    }

    public void verifyAlert(String textAlertExpect, String colorCodeExpect){
        //verify text
        labelAlertError.waitForElementToBeDisplay();
        String textActual = labelAlertError.getText();
        Assert.assertEquals(textActual,textAlertExpect,String.format("Failed, Text expect %s but found %s",textAlertExpect,textActual));

        //verify color text
        String color = labelAlertError.getCSSValueOfElement("color");
        String colorCodeActual = Color.fromString(color).asHex();
        Assert.assertEquals(colorCodeActual,colorCodeExpect,String.format("Failed, Text expect %s but found %s",colorCodeExpect,colorCodeActual));
    }

    public void fillPassword(String password){
        textboxPasswd.waitForElementToBeDisplay();
        textboxPasswd.click();
        if (password!=null){
            textboxPasswd.sendClearText(password);
            waitForJSToComplete();
        }
        buttonNext.click();
    }
}
