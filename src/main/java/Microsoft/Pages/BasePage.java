package Microsoft.Pages;

import org.openqa.selenium.By;

public abstract class BasePage extends Core.Selenium.BasePage {
    public BasePage(By by, String name, boolean assertOpen) {
        super(by, name, assertOpen);
    }

}
