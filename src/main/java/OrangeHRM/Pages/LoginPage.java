package OrangeHRM.Pages;

import Core.Selenium.BasePage;
import Core.Selenium.Button;
import Core.Selenium.Label;
import Core.Selenium.Textbox;
import org.openqa.selenium.By;
import org.openqa.selenium.support.Color;
import org.testng.Assert;

public class LoginPage extends BasePage {

    private static final By by = new By.ByXPath("//div[@class='orangehrm-login-branding']");
    private static final String name = "Login page";

    // locator
    private final String name_textboxUserName = "username";
    private final String name_textboxPasswd = "password";
    private final String xpath_buttonLogin = "//button[@type='submit']";
    private final String xpath_labelAlertError = "//div[@role='alert']//p[contains(@class,'alert-content-text')]";

    //element
    private final Textbox textboxUserName = new Textbox(By.name(name_textboxUserName), "user name");
    private final Textbox textboxPasswd = new Textbox(By.name(name_textboxPasswd), "password");
    private final Button buttonLogin = new Button(By.xpath(xpath_buttonLogin), "login button");
    private final Label labelAlertError = new Label(By.xpath(xpath_labelAlertError), "alert error");

    public LoginPage(boolean assertPageOpen) {
        super(by, name, assertPageOpen);
    }

    public void inputUserName(String userName){
        textboxUserName.waitForElementToBeDisplay();
        textboxUserName.click();
        textboxUserName.sendClearText(userName);
        waitForJSToComplete();
    }

    public void inputPassword(String password){
        textboxPasswd.waitForElementToBeDisplay();
        textboxPasswd.click();
        textboxPasswd.sendClearText(password);
        waitForJSToComplete();
    }

    public void clickLogin(){
        buttonLogin.waitForClickable();
        buttonLogin.click();
    }

    public void verifyAlert(String textAlertExpect, String colorCodeExpect){
        //verify text
        labelAlertError.waitForElementToBeDisplay();
        String textActual = labelAlertError.getText();
        Assert.assertEquals(textActual,textAlertExpect,String.format("Failed, Text expect %s but found %s",textAlertExpect,textActual));

        //verify color text
        String color = labelAlertError.getCSSValueOfElement("color");
        String colorCodeActual = Color.fromString(color).asHex();
        Assert.assertEquals(colorCodeActual,colorCodeExpect,String.format("Failed, Text expect %s but found %s",colorCodeExpect,colorCodeActual));
    }
}
