package OrangeHRM.Pages;

import Core.Selenium.*;
import org.openqa.selenium.By;
import org.testng.Assert;

public class SearchPage extends BasePage {
    private final static By by = new By.ByXPath("//button[@type='submit']");
    private final static String name = "Search page";

    //locator
    private final String xpath_toastContent = "//div[contains(@class,'oxd-toast-content')]/p[contains(@class,'toast-message')]";
    private final String xpath_buttonSearch = "//button[@type='submit']";
    private final String xpath_textboxUserName = "//label[contains(text(),'name')]/parent::div/following-sibling::div/input[contains(@class,'oxd-input')]";
    private final String xpath_selectUserRole = "//label[contains(text(),'Role')]/parent::div/following-sibling::div//div[contains(text(),'Select')]";

    //element
    private final Label labelToastContent = new Label(By.xpath(xpath_toastContent), "toast content");
    private final Button buttonSearch = new Button(By.xpath(xpath_buttonSearch), "search");
    private final Textbox textboxUserName = new Textbox(By.xpath(xpath_textboxUserName), "user name");
    private final Select selectUserRole = new Select(By.xpath(xpath_selectUserRole), "user role");


    public SearchPage(boolean assertPageOpen) {
        super(by, name, assertPageOpen);
    }

    public void searchUser(String name, String role) {
        //input name
        textboxUserName.waitForElementToBeDisplay();
        textboxUserName.click();
        textboxUserName.sendClearText(name);
        waitForJSToComplete();

        //select role
        selectUserRole.click();
        ListOfElements listOfElements = new ListOfElements(By.xpath("//div[@role='listbox']/div[@role='option']"), "list role");
        for (int i = 0; i < listOfElements.getListOfElement().size(); i++) {
            if (listOfElements.getListOfElement().get(i).getText().equalsIgnoreCase(role)) {
                listOfElements.getListOfElement().get(i).click();
                break;
            }
        }

        buttonSearch.waitForClickable();
        buttonSearch.click();
    }

    public void verifyToastMessage(String toastContent) {
        labelToastContent.waitForElementToBeDisplay();
        Assert.assertEquals(labelToastContent.getText(), toastContent, String.format("Failed, Message expected is %s but found %s", toastContent, labelToastContent.getText()));
    }
}
