package OrangeHRM.Pages;

import Core.Selenium.BasePage;
import Core.Selenium.Label;
import org.openqa.selenium.By;

public class HomePage extends BasePage {
    private static final By by = new By.ByXPath("//div[@class='oxd-topbar-header-userarea']");
    private static final String name = "home page";

    //locator
    private final String xpath_labelAdminMenu = "//ul[@class='oxd-main-menu']/li/a/span[text()='Admin']";
    private final String xpath_labelBuzzMenu = "//ul[@class='oxd-main-menu']/li/a/span[text()='Buzz']";

    //element
    private final Label labelAdminMenu = new Label(By.xpath(xpath_labelAdminMenu), "admin");
    private final Label labelBuzzMenu = new Label(By.xpath(xpath_labelBuzzMenu), "new feed");

    public HomePage(boolean assertPageOpen) {
        super(by, name, assertPageOpen);
    }

    public void goToAdminManager(){
        labelAdminMenu.waitForElementToBeDisplay();
        labelAdminMenu.click();
    }

    public void goToNewfeedManager(){
        labelBuzzMenu.waitForElementToBeDisplay();
        labelBuzzMenu.click();
    }
}
