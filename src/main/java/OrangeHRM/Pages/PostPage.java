package OrangeHRM.Pages;

import Core.Selenium.*;
import org.openqa.selenium.By;
import org.testng.Assert;

public class PostPage extends BasePage {
    private static final By by = new By.ByXPath("//textarea[@class='oxd-buzz-post-input']");
    private static final String name = "new feed";

    //locator
    private final String xpath_inputPost = "//textarea[@class='oxd-buzz-post-input']";
    private final String xpath_buttonPost = "//button[@type='submit']";
    private final String id_toastPopup = "oxd-toaster_1";
    private final String xpath_listBodyPost = "//div[contains(@class,'post-body')]/p";

    //element
    private final Textbox textboxInputPost = new Textbox(By.xpath(xpath_inputPost), "input");
    private final Button buttonPost = new Button(By.xpath(xpath_buttonPost), "post");
    private final Label labelToast = new Label(By.id(id_toastPopup), "toast");
    private final ListOfElements listOfElements = new ListOfElements(By.xpath(xpath_listBodyPost), "list content");

    public PostPage(boolean assertPageOpen) {
        super(by, name, assertPageOpen);
    }

    public void inputContent(String content) {
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        textboxInputPost.waitForElementToBeDisplay();
        textboxInputPost.click();
        textboxInputPost.sendClearText(content);
        waitForJSToComplete();
    }

    public void clickPostButton() {
        buttonPost.waitForClickable();
        buttonPost.click();
        waitForJSToComplete();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public void verifyContentAfterPost(String contentExpected) {
        String contentActual = listOfElements.getListOfElement().get(0).getText();
        Assert.assertEquals(contentActual, contentExpected, "Failed, Content is not compare...");
    }

}
