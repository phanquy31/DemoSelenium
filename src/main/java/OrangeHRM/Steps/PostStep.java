package OrangeHRM.Steps;

import OrangeHRM.Pages.HomePage;
import OrangeHRM.Pages.PostPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

public class PostStep {
    @And("Click menu buzz and go to new feed page")
    public void clickMenuBuzzAndGoToNewFeedPage() {
        HomePage homePage = new HomePage(true);
        homePage.goToNewfeedManager();
    }

    @And("Post new feed with content {string}")
    public void postNewFeedWithContent(String content) {
        PostPage postPage = new PostPage(true);
        postPage.inputContent(content);
        postPage.clickPostButton();
    }


    @Then("Verify content newfeed after post compare content {string}")
    public void verifyContentNewfeedAfterPostCompare(String content) {
        PostPage postPage = new PostPage(true);
        postPage.verifyContentAfterPost(content);
    }
}
