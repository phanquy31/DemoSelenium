package OrangeHRM.Steps;

import OrangeHRM.Pages.HomePage;
import OrangeHRM.Pages.SearchPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

public class SearchStep {

    @And("Click menu admin and go to admin manager page")
    public void clickMenuAdminAndGoToAdminManagerPage() {
        HomePage homePage = new HomePage(true);
        homePage.goToAdminManager();
    }

    @And("Search user with name {string} role {string}")
    public void searchUserWithNameRole(String name, String role) {
        SearchPage searchPage = new SearchPage(true);
        searchPage.searchUser(name, role);
    }

    @Then("Verify toast popup is show content {string}")
    public void verifyToastPopupIsShowContent(String toastContent) {
        SearchPage searchPage = new SearchPage(true);
        searchPage.verifyToastMessage(toastContent);
    }


}
