package OrangeHRM.Steps;

import OrangeHRM.Pages.HomePage;
import OrangeHRM.Pages.LoginPage;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

public class LoginStep {

    @When("I open url then input with user name {string} password {string} and login")
    public void iInputUserNameAndPassword(String userName, String password) {
        LoginPage loginPage = new LoginPage(true);
        loginPage.inputUserName(userName);
        loginPage.inputPassword(password);
        loginPage.clickLogin();
    }

    @Then("Verify text alert is {string} and color code {string}")
    public void verifyTextAlertIsWithColorCode(String textAlert, String colorCode) {
        LoginPage loginPage = new LoginPage(true);
        loginPage.verifyAlert(textAlert,colorCode);
    }

    @Then("Verify login success and go to home page")
    public void verifyLoginSuccess() {
        HomePage homePage = new HomePage(true);
        Assert.assertTrue(homePage.isOnPage(),"Login fail.....");
    }

}
