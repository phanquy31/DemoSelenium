package Core.Selenium;

import Core.BaseEntities;
import Core.Support.Selenium.Waiter;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;

import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class BaseElement extends BaseEntities {
    private final static Logger LOGGER = Logger.getLogger(BaseElement.class.getCanonicalName());
    private int attempts = 0;
    private int time = 2;
    private By by;
    private String elementName;
    private String elementType;

    public BaseElement(By by, String type, String name) {
        this.by = by;
        this.elementType = type;
        this.elementName = name;
    }

    protected WebElement getElement() {
        WebElement webElement = null;
        while (attempts < time) {
            try {
                webElement = driver.findElement(by);
                break;
            } catch (StaleElementReferenceException e) {
                e.getMessage();
            }
            attempts++;
        }
        return webElement;
    }

    public void waitForElementToBeDisplay() {
        LOGGER.log(Level.INFO, String.format(">>>>> Waiting for [%s] name [%s] to be displayed", elementType, elementName));
        while (attempts < time) {
            try {
                Waiter.waitForElementToBeDisplay(driver.findElement(by));
                break;
            } catch (StaleElementReferenceException | NoSuchElementException e) {
                e.getMessage();
            }
            attempts++;
        }
    }

    public void click() {
        LOGGER.log(Level.INFO, String.format(">>>>> Click on [%s] name [%s]", elementType, elementName));
        while (attempts < time) {
            try {
                driver.findElement(by).click();
                break;
            } catch (StaleElementReferenceException e) {
                e.getMessage();
            }
            attempts++;
        }
    }

    public void hoverMouse() {
        LOGGER.log(Level.INFO, String.format(">>>>> Mouse hover on [%s] name [%s]", elementType, elementName));
        Actions action = new Actions(driver);
        while (attempts < time) {
            try {
                action.moveToElement(driver.findElement(by)).perform();
                Waiter.wait(1);
                break;
            } catch (StaleElementReferenceException e) {
                e.getMessage();
            }
            attempts++;
        }
    }

    public String getText() {
        LOGGER.log(Level.INFO, String.format(">>>>> Get text of [%s] name [%s]", elementType, elementName));
        String returnText = "";
        while (attempts < time) {
            try {
                returnText = driver.findElement(by).getText();
                break;
            } catch (StaleElementReferenceException e) {
                e.getMessage();
            }
            attempts++;
        }
        return returnText;
    }

    public String getCSSValueOfElement(String value) {
        String returnText = "";
        while (attempts < time) {
            try {
                returnText = driver.findElement(by).getCssValue(value);
                break;
            } catch (StaleElementReferenceException e) {
                e.getMessage();
            }
            attempts++;
        }
        return returnText;
    }

    public WebElement getWebElement() {
        WebElement webElement = null;
        while (attempts < time) {
            try {
                webElement = driver.findElement(by);
                break;
            } catch (StaleElementReferenceException e) {
                e.getMessage();
            }
            attempts++;
        }
        return webElement;
    }

    protected String getElementName() {
        return this.elementName;
    }

    protected String getElementType() {
        return this.elementType;
    }

    protected By getBy() {
        return this.by;
    }
}
