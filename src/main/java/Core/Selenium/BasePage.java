package Core.Selenium;

import Core.BaseEntities;
import Core.Support.Selenium.BrowserManager;
import Core.Support.Selenium.Waiter;
import org.openqa.selenium.*;
import org.testng.Assert;

public abstract class BasePage extends BaseEntities {
    private final String pageName;
    private final By pageLocator;

    public BasePage(By pageLocator, String pageName, boolean assertPageOpen) {
        this.pageLocator = pageLocator;
        this.pageName = pageName;
        if (assertPageOpen) {
            waitForJSToComplete();
            waitForPageLoadComplete();
            Assert.assertTrue(isOnPage());
        }
    }

    public boolean isOnPage() {
        try {
            return driver.findElement(pageLocator).isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    public void waitForJSToComplete() {
        Waiter.waitForJSToComplete();
    }

    public void waitForPageLoadComplete() {
        int counter = 0;
        int attempt = 2;
        while (counter < attempt) {
            try {
                Waiter.waitForElementToBeDisplay(driver.findElement(pageLocator));
                break;
            } catch (NoSuchElementException | StaleElementReferenceException e) {
                System.out.println("--> Something wrong while waiting for page to loaded completely, trying again ...");
                e.printStackTrace();
                BrowserManager.getInstance().refreshPage();
                counter++;
            }
        }
    }

    public void refresh() {
        BrowserManager.getInstance().refreshPage();
    }

    public String getPageName() {
        return this.pageName;
    }

    public By getPageLocator() {
        return this.pageLocator;
    }
}
