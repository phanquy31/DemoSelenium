package Core.Selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import java.util.logging.Logger;

public class Textbox extends BaseElement {
    private final static Logger LOGGER = Logger.getLogger(Textbox.class.getCanonicalName());
    private static String type = "TextBox";
    private int attempts = 0;
    private int time = 2;

    public Textbox(By by, String name) {
        super(by, type, name);
    }

    public void sendClearText(String text) {
        while (attempts < time) {
            try {
                super.getElement().clear();
                Thread.sleep(500);
                super.getElement().sendKeys(text);
                break;
            } catch (StaleElementReferenceException | InterruptedException e) {
                e.printStackTrace();
            }
            attempts++;
        }
    }

}
