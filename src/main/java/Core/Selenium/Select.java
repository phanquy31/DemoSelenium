package Core.Selenium;

import org.openqa.selenium.By;
import java.util.logging.Logger;

public class Select extends BaseElement {
    private final static Logger LOGGER = Logger.getLogger(Select.class.getCanonicalName());

    private static String type = "Select";

    public Select(By by, String name) {
        super(by, type, name);
    }


}
