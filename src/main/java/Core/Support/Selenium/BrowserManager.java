package Core.Support.Selenium;

import Core.BaseEntities;

public class BrowserManager extends BaseEntities {
    private static BrowserManager instance;

    private BrowserManager() {}

    public static BrowserManager getInstance() {
        if (instance == null) {
            instance = new BrowserManager();
        }
        return instance;
    }

    public void navigateToUrl(String url) {
        driver.get(url);
    }

    public void refreshPage() {
        driver.navigate().refresh();
    }

    public String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

}
