package Core.Support.Selenium;

import Core.BaseEntities;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;
import java.time.Duration;

public class BrowserFactory extends BaseEntities {
    static final String CHROME = "CHROME";
    static final String FIREFOX = "FIREFOX";
    static final String SAFARI = "SAFARI";
    static final String EDGE = "EDGE";

    public static void initBrowser() {
        String browser = "";
        try {
            browser = BaseEntities.getSeleniumBrowser();
        } catch (Exception e) {
            e.printStackTrace();
        }

        switch (browser.toUpperCase()) {
            case CHROME:
                driver = new ChromeDriver(initChromeOptions());
                break;
            case FIREFOX:
                driver = new FirefoxDriver(initFirefoxOptions());
                break;
            case SAFARI:
                driver = new SafariDriver(initSafariOptions());
                break;
            case EDGE:
                driver = new EdgeDriver(initEdgeOptions());
                break;
            default:
                throw new IllegalStateException(browser + " is not supported yet.");
        }
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(BaseEntities.getPageLoadTimeOut()));
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(BaseEntities.getImplicitTimeOut()));
    }

    public static void killBrowser() {
        driver.quit();
    }

    private static ChromeOptions initChromeOptions() {
        ChromeOptions options = new ChromeOptions();
        options.setBrowserVersion("122");
        return options;
    }

    private static SafariOptions initSafariOptions() {
        SafariOptions options = new SafariOptions();
        return options;
    }

    private static FirefoxOptions initFirefoxOptions() {
        FirefoxOptions options = new FirefoxOptions();
        return options;
    }

    private static EdgeOptions initEdgeOptions(){
        EdgeOptions options = new EdgeOptions();
        return options;
    }

}
