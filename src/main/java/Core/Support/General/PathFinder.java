package Core.Support.General;

import Core.BaseEntities;

import java.io.File;
import java.util.logging.Logger;

public class PathFinder extends BaseEntities {
    private final static String currentWorkingDirectory = System.getProperty("user.dir");
    private final static Logger LOGGER = Logger.getLogger(PathFinder.class.getCanonicalName());

    // Folder
    private static final String configFolderName = "config";
    private static final String DATA = "data";
    private static final String TMP = "tmp";
    private static final String DOWNLOAD = "download";

    public static String getPathToTimeOutConfiguration() {
        return currentWorkingDirectory + File.separator + configFolderName + File.separator + "timeout.properties";
    }

    public static String getDataFolderPath() {
        return currentWorkingDirectory + File.separator + DATA + File.separator;
    }

    public static String getDownloadFolderPath() {
        return getDataFolderPath() + DOWNLOAD;
    }

    public static String getTmpFolderPath() {
        return currentWorkingDirectory + File.separator + TMP + File.separator;
    }

}
