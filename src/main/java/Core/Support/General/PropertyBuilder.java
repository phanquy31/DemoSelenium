package Core.Support.General;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class PropertyBuilder {
    private static Properties properties;

    public static Properties getTimeOutConfig() {
        Properties timeoutProperties = new Properties();
        try {
            BufferedReader ip = new BufferedReader(new InputStreamReader(new FileInputStream(PathFinder.getPathToTimeOutConfiguration()), StandardCharsets.UTF_8));
            timeoutProperties.load(ip);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return timeoutProperties;
    }

}
