package Core.Support.General;

import Core.BaseEntities;
import Core.Support.Selenium.BrowserFactory;
import Core.Support.Selenium.BrowserManager;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;

import java.util.logging.Logger;

public class Hooks extends BaseEntities {
    private final static Logger LOGGER = Logger.getLogger(Hooks.class.getCanonicalName());
    private final String urlMCS = "https://www.microsoft.com/en-us/";
    private final String urlOrangeHRM = "https://opensource-demo.orangehrmlive.com/web/index.php/auth/login";

    /*
        TestNG config
     */
    @Before("@selenium")
    public void beforeWeb() {
        BrowserFactory.initBrowser();
        try {
            BrowserManager.getInstance().navigateToUrl(urlOrangeHRM);
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }

    @After("@selenium")
    public void afterWeb(Scenario scenario) {
        if (scenario.isFailed()) {
            final byte[] screenShot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenShot, "image/png", scenario.getName());
        }
        driver.quit();
    }

}
