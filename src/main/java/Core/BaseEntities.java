package Core;

import Core.Support.General.PropertyBuilder;
import io.cucumber.java.Scenario;
import org.openqa.selenium.WebDriver;
import java.util.Arrays;

public abstract class BaseEntities {
    protected static WebDriver driver;
    private final static String[] seleniumValidBrowser = new String[]{"CHROME", "FIREFOX", "SAFARI", "EDGE"};
    private static Scenario scenario;

    // selenium = window ,chrome, language, headless
    private static String[] getSeleniumConfig() {
        return System.getProperty("selenium").split(",");
    }

    protected static String getSeleniumBrowser() throws Exception {
        String browser = getSeleniumConfig()[0];
        if (Arrays.asList(seleniumValidBrowser).contains(browser.toUpperCase())) {
            return browser;
        } else {
            throw new Exception("Invalid Browser, currently we only support: chrome ...");
        }
    }

    protected static int getPageLoadTimeOut() {
        return Integer.parseInt(PropertyBuilder.getTimeOutConfig().getProperty("page.load.time.out"));
    }

    protected static int getImplicitTimeOut() {
        return Integer.parseInt(PropertyBuilder.getTimeOutConfig().getProperty("implicitly.time.out"));
    }

    protected static int getElementTimeOut() {
        return Integer.parseInt(PropertyBuilder.getTimeOutConfig().getProperty("element.time.out"));
    }

    public static Scenario getScenario() {
        return scenario;
    }

    protected static void setScenario(Scenario scenario) {
        BaseEntities.scenario = scenario;
    }

}