@selenium @search

Feature: As user I want to search product

  @search-1
  Scenario: Search product and see suggest product
    Given Open url and click to search
    When Search product with key "Sureface"
    Then Verify the product name "Sureface Pro 9" exists in the suggested products list
    And Click search and go to search page link "https://www.microsoft.com/en-us/search/explore?q=Sureface"

