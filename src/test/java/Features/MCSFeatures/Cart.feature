@selenium @cart

Feature: As user I want to check product into cart

  @cart-1
  Scenario: Check the shopping cart when there are no products in the cart
    Given Open url and hover mouse into cart icon
    And See show content is "0 items in shopping cart"
    And Click to cart detail
    Then Detail description show text "Your cart is empty."