@selenium @login

Feature: As user I want to Check login function

  @login-1
  Scenario Outline: Login failed when entering incorrect user name
    Given Open url and go to login page
    When Enter my user name "<userName>"
    Then Verify text alert is "<text>" and color code "<colorCode>"
    Examples:
      | userName | text                                                                              | colorCode |
      |          | Enter a valid email address, phone number, or Skype name.                         | #e81123   |
      | 123      | Enter a valid email address, phone number, or Skype name.                         | #e81123   |
      | abc      | That Microsoft account doesn't exist. Enter a different account or get a new one. | #e81123   |

  @login-2
  Scenario Outline: Login failed when entering incorrect password
    Given Open url and go to login page
    When Enter my user name "<userName>"
    And Enter my password "<password>"
    Then Verify text alert is "<text>" and color code "<colorCode>"
    Examples:
      | userName           | password | text                                                                        | colorCode |
      | hoangquy@gmail.com |          | Please enter the password for your Microsoft account.                       | #e81123   |
      | hoangquy@gmail.com | 123      | Your account or password is incorrect. If you don't remember your password, | #e81123   |