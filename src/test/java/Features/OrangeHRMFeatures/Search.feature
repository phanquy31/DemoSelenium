@selenium @HRM-search @HRM

Feature: As admin I search user into system

  @HRM-search-1
  Scenario Outline: Search for users that are not in the system
    When I open url then input with user name "<userName>" password "<password>" and login
    And Click menu admin and go to admin manager page
    And Search user with name "<name>" role "<role>"
    Then Verify toast popup is show content "No Records Found"
    Examples:
      | userName | password | name | role  |
      | Admin    | admin123 | abc  | Admin |