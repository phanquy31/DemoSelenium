@selenium @HRM-login @HRM

Feature: As user I want to Check login function

  @HRM-login-1
  Scenario Outline: Login failed when entering incorrect user name & password
    When I open url then input with user name "<userName>" password "<password>" and login
    Then Verify text alert is "Invalid credentials" and color code "#eb0910"
    Examples:
      | userName | password |
      | abc      | 123      |
      | Admin    | 123      |
      | abcv     | admin123 |

  @HRM-login-2
  Scenario Outline: Login success when entering correct user name & password
    When I open url then input with user name "<userName>" password "<password>" and login
    Then Verify login success and go to home page
    Examples:
      | userName | password |
      | Admin    | admin123 |