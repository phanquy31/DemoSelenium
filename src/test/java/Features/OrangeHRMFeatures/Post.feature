@selenium @HRM-post @HRM

Feature: As user I post new feed in my wall

  @HRM-post-1
  Scenario: Search for users that are not in the system
    When I open url then input with user name "Admin" password "admin123" and login
    And Click menu buzz and go to new feed page
    And Post new feed with content "Bai test demo selenium voi web"
    Then Verify content newfeed after post compare content "Bai test demo selenium voi web"