import Core.Support.General.ReportGenerator;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.AfterTest;

@CucumberOptions(
        features = {"src/test/java/Features"},
        monochrome = true,
        strict = true,

        // if run Microsoft web pleas set glue = {"Microsoft.Steps", "Core.Support"}
        glue = {"OrangeHRM.Steps", "Core.Support"},
        tags = "@HRM-search-1 and not @skip",
        plugin = {
                "html:target/result",
                "pretty",
                "json:target/test-classes/reports/result.json"
        }
)

public class CucumberRunner extends AbstractTestNGCucumberTests {
        @AfterTest
        public void generateCucumberReportAndCleanUpEnv() throws Exception {
                System.out.println("Generating cucumber report ...");
                ReportGenerator.generateCucumberReport();
                System.out.println("Generating cucumber report success...");
        }
}